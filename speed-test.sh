#!/bin/bash
# author:   Hieu Duong
# email:    me@hieuduong.com
# version:  v1.05

MOUNT_DIR=/mnt/ssd
SSD=$1
TEMP=/tempfile

if [ ! -d "${MOUNT_DIR}" ]
then
    echo "Mounting directory did not exist. Create directory..."
    mkdir "${MOUNT_DIR}"
else
    echo "Mounting directory exists..."
fi

mount | grep ${SSD} &> /dev/null
if [ $? != 0 ]
then
    echo "Disk is not mounted. Mounting disk..."
    sudo mount ${SSD}1 ${MOUNT_DIR}
else
    echo "Disk has been mounted."
fi

echo "Starting first benchmark"
sudo hdparm -Tt "${SSD}"

sleep 2
echo "Starting second benchmark"
echo "Write test"
sudo dd if=/dev/zero of=$MOUNT_DIR$TEMP bs=1M count=1024 conv=fdatasync,notrunc
sleep 2
echo "Read test (without buffer)"
echo 3 | sudo tee /proc/sys/vm/drop_caches
sudo dd if=$MOUNT_DIR$TEMP of=/dev/null bs=1M count=1024
sleep 2
echo "Read test (with buffer)"
sudo dd if=$MOUNT_DIR$TEMP of=/dev/null bs=1M count=1024

sudo rm $MOUNT_DIR$TEMP

echo "End test."
